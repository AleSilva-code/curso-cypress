
describe ("Tickets",() => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));
    it.only("fills all the text inut fields", () =>{
        const firstName ="Ale";
        const lastName = "Silva";
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("Ale@gmail.com");
        cy.get("#requests").type("LC");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });
    it ("Select two tickets", ()=> {
        cy.get("#ticket-quantity").select("2");
    });
    it("Select 'vip' ticket type", ()=> {
        cy.get("#vip").check();
    });
    it("Select social media check box", ()=> {
        cy.get("#social-media").check();
    });
    it("Selects 'friend', and 'publication', then uncheck 'friend'", ()=> {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });
    it("has 'TICKETBOX' header's heading",()=>{

        cy.get("header h1").should("contain", "TICKETBOX");
    });
    it("Alert on invalid email",()=>{

        cy.get("#email")
        .as("email")
        .type("ale-gmail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
        .clear()
        .type("ale@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    });
    it("fills and reset the form", ()=>{
        const firstName ="Ale";
        const lastName = "Silva";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("Ale@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("LC");

        cy.get(".agreement p").should (
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );
        cy.get("#agree").click();
        cy.get("#signature").type("fullName");

        cy.get ("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });
    it.only("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "João",
            lastName: "Silva",
            email: "joaosilva@gmail.com "
        };   

        cy.fillMandatoryFields(customer);

        cy.get ("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });
});


